
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.Test;
import org.yari.core.Context;
import org.yari.core.ExecutionResult;
import org.yari.core.RuleResult;

/*
 * This file is part of Yari Editor.
 *
 * Yari Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari Editor.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * You can use this class to generate test JSON files for the Inspector.
 */
public class MakeTestJSON {

    private final List<Boolean> bools = Arrays.asList(true, false);
    private final List<Boolean> exbools = Arrays.asList(true, false, false, false, false, false, false);

    @Test
    public void testResultGson() {
        // -- Setup -- //
        int numberOfResultsToCreate = 50;
        int numberOfPossibleRules = 10;
        int filesToMake = 5;
        String fileSaveDir = "ENTER LOCATION TO SAVE FILES";

        for (int x = 0; x < filesToMake; x++) {
            // -- Randomly create rule results --
            String fileSaveLocation = fileSaveDir + "test" + x + ".json";
            Instant then = Instant.now();
            ExecutionResult er = new ExecutionResult();
            Random randomizer = new Random();
            for (int i = 0; i < numberOfResultsToCreate; i++) {
                RuleResult rr = new RuleResult(null);
                rr.setStartTime(Instant.now());
                Map<String, Object> testMap = new HashMap<>();
                testMap.put(1 + "", "test input data");
                Context context = new Context(testMap);

                rr.setRan(bools.get(randomizer.nextInt(bools.size())));

                rr.setRuleName("Test Rule " + (randomizer.nextInt(numberOfPossibleRules - 1 + 1) + 1) + "");

                rr.setDuration(Duration.between(then, Instant.now()));
                if (bools.get(randomizer.nextInt(bools.size()))) {
                    rr.appendLog("Test log message.");
                }

                if (exbools.get(randomizer.nextInt(exbools.size()))) {
                    try {
                        throw new Exception("Test Exception.");
                    } catch (Exception e) {
                        e.getStackTrace();
                        rr.setEx(e);
                    }
                }

                er.addRuleResult(rr);
            }

            // --- Print JSON --- //
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(er);
            File file = new File(fileSaveLocation);
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(json);
                // Write your data
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
