/*
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.yari.core.ExecutionResult;
import org.yari.core.RuleResult;
import org.yari.inspector.controller.*;
import org.yari.inspector.model.Statistics;
import org.yari.inspector.utils.Globals;
import org.yari.yaripublisher.Publisher;

public class Inspector extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private CamelContext camelContext;
    private BooleanProperty connected = new SimpleBooleanProperty();
    private OverviewController monitorController;

    private Stage primaryStage;
    private BorderPane rootLayout;
    private ObservableList<RuleResult> ruleResults = FXCollections.observableArrayList();

    private Statistics statistics;

    private RuleStatisticsController statisticsController;

    // --- Getters and Setters --- //
    public BooleanProperty getConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected.set(connected);
    }

    public OverviewController getMonitorController() {
        return monitorController;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ObservableList<RuleResult> getRuleResults() {
        return ruleResults;
    }

    public void setRuleResults(ObservableList<RuleResult> ruleResults) {
        this.ruleResults = ruleResults;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public RuleStatisticsController getStatisticsController() {
        return statisticsController;
    }

    /**
     * Loads a JSON file containing RuleResult objects into the table.
     *
     * @param file the file to load.
     */
    public void loadJSONFile(File file) {

        try {
            ExecutionResult result;
            try (FileReader reader = new FileReader(file)) {
                result = new Gson().fromJson(reader, ExecutionResult.class);
            }
            for (RuleResult ruleResult : result.getRuleResults()) {
                ruleResults.add(ruleResult);
            }

            reloadRuleStatistics();
            monitorController.updateOverallChart();

        } catch (Exception e) {
            showErrorAlert("Error", "Error Opening File", "The JSON file is invalid, it could not be loaded.");
        }
    }

    /**
     * Processes any incoming data from Yari core, and adds it to the JavaFX list.
     *
     * @param json The incoming JSON data from the core.
     */
    public void processIncomingData(String json) {

        // Convert the incoming data from JSON to ExecutionResult
        Gson gson = new Gson();
        ExecutionResult result = gson.fromJson(json, ExecutionResult.class);

        Platform.runLater(() -> {
            for (RuleResult ruleResult : result.getRuleResults()) {
                ruleResults.add(ruleResult);
            }
            statistics.addExecutionResult(result);
            monitorController.updateOverallChart();
        });
    }

    /**
     * Recreates rule statistics from list of RuleResults.
     */
    public void reloadRuleStatistics() {
        Statistics stats = new Statistics();

        for (RuleResult ruleResult : ruleResults) {
            stats.addRuleResult(ruleResult);
        }

        this.statistics = stats;
    }

    /**
     * Saves any loaded results in a single JSON file.
     *
     * @param file JSON file containing all loaded rule results.
     */
    public void saveResultsToFile(File file) {
        ExecutionResult er = new ExecutionResult();
        for (RuleResult rr : ruleResults) {
            er.addRuleResult(rr);
        }
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(er);
        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(json);
            }
        } catch (Exception e) {
            showErrorAlert("Error", "Could Not Save", "Could not save the file to the specified location.");
        }
    }

    /**
     * Listens on JMS for incoming ExecutionResults from Yari core.
     *
     * @throws Exception
     */
    public void setupListener() throws Exception {
        // Setup Camel
        camelContext = new DefaultCamelContext();
        camelContext.addComponent("activemq", ActiveMQComponent.activeMQComponent("tcp://localhost:61616"));

        Publisher publish = new Publisher();
        publish.startListen();

        // Route incoming data to processIncomingData method
        camelContext.addRoutes(new RouteBuilder() {
            public void configure() {
                from("activemq:topic:yari").bean(Inspector.this, "processIncomingData");
            }

        });

        camelContext.start();

    }

    /**
     * Shows a standard confirmation dialog, with a runnable object to complete if okay is clicked.
     *
     * @param title dialog title.
     * @param header dialog header.
     * @param content dialog content.
     * @param ifOkayClicked what happens when okay is clicked.
     */
    public void showConfirmationDialog(String title, String header, String content, Runnable ifOkayClicked) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Globals.YARI_LOGO);
        alert.getDialogPane().getStylesheets().add("/css/YariTheme.css");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            ifOkayClicked.run();
        }
    }

    /**
     * Helper method to create error alert dialogs.
     *
     * @param title title text.
     * @param header header text.
     * @param content content text.
     */
    public void showErrorAlert(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Globals.YARI_LOGO);
        alert.getDialogPane().getStylesheets().add("/css/YariTheme.css");
        alert.showAndWait();
    }

    /**
     * Shows a standard alert dialog.
     *
     * @param title title text.
     * @param header header text.
     * @param content content text.
     */
    public void showInformationAlert(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Globals.YARI_LOGO);
        alert.getDialogPane().getStylesheets().add("/css/YariTheme.css");
        alert.showAndWait();
    }

    /**
     * Shows the rule overview in the rootLayout.
     */
    public void showMonitor() {
        try {
            // Load row editor
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Inspector.class.getResource("/fxml/Overview.fxml"));
            AnchorPane monitor = (AnchorPane) loader.load();

            // Set the row editor to the center of the root layout.
            rootLayout.setCenter(monitor);

            // Give the controller access to the main app
            monitorController = loader.getController();
            monitorController.setInspector(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the rule statistics in the root layout.
     */
    public void showRuleStatistics() {
        try {
            // Load row editor
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Inspector.class.getResource("/fxml/RuleStatistics.fxml"));
            AnchorPane ruleStatsPane = (AnchorPane) loader.load();

            // Set the row editor to the center of the root layout.
            rootLayout.setCenter(ruleStatsPane);

            // Give the controller access to the main app
            statisticsController = loader.getController();
            statisticsController.setInspector(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Required method to start the program. Sets the primaryStage, stage title, and icon. Loads the rootLayout, then the overview within the rootLayout.
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Yari Inspector");

        // Set Application Icon
        this.primaryStage.getIcons().add(Globals.YARI_LOGO);

        // Set initial connection status.
        this.connected.set(false);
        this.statistics = new Statistics();

        initRootLayout();
        showMonitor();

    }

    public void stopListener() throws Exception {
        camelContext.stop();
    }

    /**
     * Initializes the rootLayout for the program.
     */
    protected void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Inspector.class.getResource("/fxml/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            rootLayout.getStylesheets().add("/css/YariTheme.css");

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app for menu controls.
            RootLayoutController controller = loader.getController();
            controller.setInspectorMain(this);

            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
