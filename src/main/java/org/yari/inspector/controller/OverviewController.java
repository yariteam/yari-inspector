/*
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.yari.core.RuleResult;
import org.yari.inspector.Inspector;

public class OverviewController {

    private ObservableList<String> comboBoxChoices = observableArrayList();

    @FXML
    private Label durationLabel;
    private final ObservableList<String> empty = FXCollections.observableArrayList();
    private final ObservableList<String> exAndLog = FXCollections.observableArrayList("Exception Stacktrace", "Logs");
    private final ObservableList<String> exStacktrace = FXCollections.observableArrayList("Exception Stacktrace");
    @FXML
    private TableColumn<RuleResult, String> exceptionCol;

    @FXML
    private Label exceptionLabel;
    @FXML
    private TextField filterField;
    private Inspector inspector;

    @FXML
    private Label logLabel;
    private final ObservableList<String> logs = FXCollections.observableArrayList("Logs");
    @FXML
    private TableColumn<RuleResult, String> nameCol;
    @FXML
    private Label nameLabel;

    @FXML
    private PieChart overallchart;
    @FXML
    private TableColumn<RuleResult, Boolean> ranCol;

    @FXML
    private Label ranLabel;
    @FXML
    private TableView<RuleResult> resultsTable;

    private RuleResult selectedResult;
    @FXML
    private TableColumn<RuleResult, String> startCol;
    @FXML
    private Label startLabel;
    @FXML
    private TextArea viewArea;
    @FXML
    private ChoiceBox<String> viewChoiceBox;

    /**
     * Filters table to only show rules with exceptions.
     */
    public void filterExceptions() {
        setupColumns();
        resultsTable.setItems(resultsTable.getItems().filtered(r -> exceptionLabel(r.getEx()).equalsIgnoreCase("yes")));
    }

    /**
     * Filters table to only show rules that did not run.
     */
    public void filterNotRan() {
        setupColumns();
        resultsTable.setItems(resultsTable.getItems().filtered(r -> String.valueOf(r.getRan()).equalsIgnoreCase("false")));
    }

    /**
     * Filters table to only show rules that ran.
     */
    public void filterRan() {
        setupColumns();
        resultsTable.setItems(resultsTable.getItems().filtered(r -> String.valueOf(r.getRan()).equalsIgnoreCase("true")));
    }

    /**
     * Links the controller to the main application, sets up the columns in the table, and initializes some pie chart data.
     *
     * @param inspector
     */
    public void setInspector(Inspector inspector) {
        this.inspector = inspector;
        setupColumns();

        // --- Overall Chart -- //
        overallchart.setTitle("Executions By Rule");
        overallchart.setLegendVisible(false);
        overallchart.setData(inspector.getStatistics().getPieChartData());

    }

    // --- Getters and Setters --- //
    public PieChart getOverallchart() {
        return overallchart;
    }

    public void setOverallchart(PieChart overallchart) {
        this.overallchart = overallchart;
    }

    public TableView<RuleResult> getResultsTable() {
        return resultsTable;
    }

    /**
     * Removes any filters from the table.
     */
    public void unfilter() {
        setupColumns();
    }

    /**
     * Updates the overall pie chart.
     */
    public void updateOverallChart() {
        overallchart.setData(inspector.getStatistics().getPieChartData());
    }

    /**
     * Creates a readable string from the list of input data retrieved from the RuleResult
     *
     * @return Formatted string of input data
     */
    private String createInput() {
        if (selectedResult.getInputData().size() > 0) {
            StringBuilder sb = new StringBuilder();
            int index = 1;
            for (String input : selectedResult.getInputData()) {
                sb.append("[Input ").append(index).append("] ").append(input).append("\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * Creates a readable string from the list of output data retrieved from the RuleResult
     *
     * @return Formatted string of output data
     */
    private String createOutput() {
        if (selectedResult.getOutputData().size() > 0) {
            StringBuilder sb = new StringBuilder();
            int index = 1;
            for (String input : selectedResult.getOutputData()) {
                sb.append("[Output ").append(index).append("] ").append(input).append("\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * Formats the exception label to "Yes" or "No" if there is an exception or not.
     *
     * @param ex The exception to examine.
     * @return formatted string.
     */
    private String exceptionLabel(Throwable ex) {
        if (ex == null) {
            return "No";
        } else {
            return "Yes";
        }
    }

    /**
     * Formates the duration object to an easier to read format.
     *
     * @param duration the duration object to format.
     * @return formatted string of the duration.
     */
    private String formatDuration(Duration duration) {
        long s = duration.getSeconds();
        long n = duration.getNano();

        return String.format("%02d:%02d.%03d", s / 60, s % 60, TimeUnit.MILLISECONDS.convert(n, TimeUnit.NANOSECONDS));

    }

    /**
     * Creates a string of the exception's stackTrace
     *
     * @param ex the exception to create a stackTrace string of.
     * @return formatted string of exception stackTrace.
     */
    private String formatException(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    /**
     * Creates a formatted string of a particular RuleResult's logs.
     *
     * @param logs the log list.
     * @return formatted string of logs.
     */
    private String formatLogs(List<String> logs) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        for (String log : logs) {
            sb.append("[Log ").append(index++).append("] ").append(log).append("\n");
        }

        return sb.toString();
    }

    /**
     * Sets the view text area to either empty, stackTrace, or Logs depending on choice box selection.
     *
     * @param view
     */
    private void setView(String view) {
        if (view == null) {
            viewArea.setText("");
        } else if (view.equalsIgnoreCase("Exception Stacktrace")) {
            viewArea.setText(formatException(selectedResult.getEx()));
        } else if (view.equalsIgnoreCase("Logs")) {
            viewArea.setText(formatLogs(selectedResult.getLog()));
        } else if (view.equalsIgnoreCase("Input")) {
            viewArea.setText(createInput());
        } else if (view.equalsIgnoreCase("Output")) {
            viewArea.setText(createOutput());
        }
    }

    /**
     * Runs when the view is initialized. Sets labels to blank, sets placeholder for table, assigns data to columns, adds selection listener to table, adds
     * selection listener to choice box.
     */
    @FXML
    private void initialize() {
        overallchart.setAnimated(false);
        nameLabel.setText("");
        ranLabel.setText("");
        startLabel.setText("");
        durationLabel.setText("");
        exceptionLabel.setText("");
        logLabel.setText("");

        resultsTable.setPlaceholder(new Text("No rule results."));

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.US)
                .withZone(ZoneId.systemDefault());

        // --- Initialize Table --- //
        nameCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRuleName()));
        ranCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().getRan()));
        startCol.setCellValueFactory(cellData -> new SimpleStringProperty(formatter.format(cellData.getValue().getTimeRan())));
        exceptionCol.setCellValueFactory(cellData -> new SimpleStringProperty(exceptionLabel(cellData.getValue().getEx())));

        resultsTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showRuleResultDetails(newValue));

        viewChoiceBox.setDisable(true);
        viewChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldvalue, newValue) -> setView(newValue));

    }

    /**
     * Sets the columns with filterable data.
     */
    private void setupColumns() {
        // -- Wrap list in a Filtered List -- //
        FilteredList<RuleResult> filteredData = new FilteredList<>(inspector.getRuleResults(), p -> true);

        // Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(result -> {
                // If filter text is empty, display all rule results.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (result.getRuleName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches for rule name.
                } else if (String.valueOf(result.getRan()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches ran status.
                } else if (String.valueOf(result.getTimeRan()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches for time ran.
                } else if (exceptionLabel(result.getEx()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches for exception.
                }
                return false; // Does not match.
            });
        });

        // Wrap the FilteredList in a SortedList.
        SortedList<RuleResult> sortedData = new SortedList<>(filteredData);

        // Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(resultsTable.comparatorProperty());

        // Add sorted (and filtered) data to the table.
        resultsTable.setItems(sortedData);
    }

    /**
     * Shows details about a selected RuleResult.
     *
     * @param ruleResult
     */
    private void showRuleResultDetails(RuleResult ruleResult) {
        if (ruleResult != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                    .withLocale(Locale.US)
                    .withZone(ZoneId.systemDefault());

            selectedResult = ruleResult;
            nameLabel.setText(ruleResult.getRuleName());
            ranLabel.setText(ruleResult.getRan() + "");
            startLabel.setText(formatter.format(ruleResult.getTimeRan()));
            durationLabel.setText(formatDuration(ruleResult.getDuration()));
            exceptionLabel.setText(exceptionLabel(ruleResult.getEx()));
            logLabel.setText(ruleResult.getLog().size() + " logs available");

            // setup choicebox
            if (ruleResult.getEx() != null & ruleResult.getLog().size() > 0) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(exAndLog);
                viewChoiceBox.setValue("Exception Stacktrace");
            } else if (ruleResult.getEx() != null & ruleResult.getLog().size() < 1) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(exStacktrace);
                viewChoiceBox.setValue("Exception Stacktrace");
            } else if (ruleResult.getEx() == null & ruleResult.getLog().size() > 0) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(logs);
                viewChoiceBox.setValue("Logs");
            } else if (ruleResult.getEx() == null & ruleResult.getLog().size() < 1) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(empty);
//                viewChoiceBox.setValue("Input");
            }

        } else {
            nameLabel.setText("");
            ranLabel.setText("");
            startLabel.setText("");
            durationLabel.setText("");
            exceptionLabel.setText("");
            logLabel.setText("");
            viewChoiceBox.setItems(empty);
            viewChoiceBox.setValue(null);
            viewChoiceBox.setDisable(true);
        }
    }

}
