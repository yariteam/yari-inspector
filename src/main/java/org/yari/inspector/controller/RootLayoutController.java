/* 
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.controller;

import java.io.File;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import org.yari.inspector.Inspector;

public class RootLayoutController {

    @FXML
    private MenuItem clearResultsMenuItem;

    private Inspector inspector;

    @FXML
    private MenuItem ruleStats;
    @FXML
    private MenuItem startListening;

    @FXML
    private MenuItem stopListeningMenuItem;

    /**
     * Links the controller to the main application.
     *
     * @param inspector
     */
    public void setInspectorMain(Inspector inspector) {
        this.inspector = inspector;

        // Disable menu items for various invalid operations.
        ruleStats.disableProperty().bind(Bindings.size(inspector.getRuleResults()).lessThan(1));
        stopListeningMenuItem.disableProperty().bind(inspector.getConnected().not());
        clearResultsMenuItem.disableProperty().bind(Bindings.size(inspector.getRuleResults()).lessThan(1));
        startListening.disableProperty().bind(inspector.getConnected());
    }

    /**
     * Clears results from the table.
     */
    @FXML
    void handleClearResults() {
        inspector.showConfirmationDialog("Confirmation", "Are you sure?", "This will clear all loaded rule executions. "
                + "Any executions not saved will be lost.", () -> {
                    inspector.getRuleResults().clear();
//                    inspector.setupOverallChart();
                    inspector.reloadRuleStatistics();
                    inspector.showMonitor();
                });

    }

    /**
     * Closes the application.
     */
    @FXML
    void handleClose() {
        inspector.getPrimaryStage().close();
    }

    /**
     * Removes filters from tables to show all entries.
     */
    @FXML
    void handleFilterAll() {
        inspector.getMonitorController().unfilter();
        if (inspector.getStatisticsController() != null) {
            inspector.getStatisticsController().unfilter();
        }
    }

    /**
     * Filters tables to only show rules that did not run.
     */
    @FXML
    void handleFilterOnlyNotRan() {
        inspector.getMonitorController().filterNotRan();
        if (inspector.getStatisticsController() != null) {
            inspector.getStatisticsController().filterNotRan();
        }
    }

    /**
     * Filters tables to only show rules that ran.
     */
    @FXML
    void handleFilterOnlyRan() {
        inspector.getMonitorController().filterRan();
        if (inspector.getStatisticsController() != null) {
            inspector.getStatisticsController().filterRan();
        }
    }

    /**
     * Shows an open dialog to open JSON files which will be loaded into the table.
     */
    @FXML
    void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        // Sets an extension filter for the FileChooser.
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show the open file dialog.
        List<File> fileList = fileChooser.showOpenMultipleDialog(inspector.getPrimaryStage());

        if (fileList != null) {
            for (File file : fileList) {
                inspector.loadJSONFile(file);
            }
        }
    }

    /**
     * Shows the save dialog to save loaded RuleResults to JSON file.
     */
    @FXML
    void handleSave() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON Files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(inspector.getPrimaryStage());

        if (file != null) {
            if (!file.getPath().endsWith(".json")) {
                file = new File(file.getPath() + ".json");
            }
            inspector.saveResultsToFile(file);
        }
    }

    /**
     * Filters table to show only rules with exceptions.
     */
    @FXML
    void handleShowExceptions() {
        inspector.getMonitorController().filterExceptions();
        if (inspector.getStatisticsController() != null) {
            inspector.getStatisticsController().filterExceptions();
        }
    }

    /**
     * Shows the rule overview page.
     */
    @FXML
    void handleShowMonitor() {
        inspector.showMonitor();
        inspector.getMonitorController().updateOverallChart();
    }

    /**
     * Shows the rule statistics page.
     */
    @FXML
    void handleShowStatistics() {
        inspector.showRuleStatistics();
    }

    /**
     * Starts a connection to Yari core and listens for rule executions.
     */
    @FXML
    void handleStartListening() {
        try {
            inspector.setupListener();
            inspector.setConnected(true);
            inspector.showInformationAlert("Success", "Connected", "Yari Inspector successfully connected to a running core and is now listening for rule executions.");
        } catch (Exception e) {
            inspector.showErrorAlert("Cannot Connect", "Cannot Connect", "Could not connect to a running instance of Yari core.");
        }
    }

    /**
     * Stops a connection to Yari core.
     */
    @FXML
    void handleStopListening() {
        try {
            inspector.stopListener();
            inspector.setConnected(false);
            inspector.showInformationAlert("Success", "Disconnected", "Yari Inspector is now disconnected from the core.");
        } catch (Exception e) {
            inspector.showErrorAlert("Error", "Cannot Stop", "Could not stop connection.");
        }
    }

}
