/*
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import org.yari.core.RuleResult;
import org.yari.inspector.Inspector;

public class RuleStatisticsController {

    @FXML
    private Label averageRunTimeLabel;
    @FXML
    private LineChart<Number, Number> durationChart;
    @FXML
    private NumberAxis durationChartXAxis;
    @FXML
    private NumberAxis durationChartYAxis;
    @FXML
    private TableColumn<RuleResult, String> durationCol;
    private final ObservableList<String> empty = FXCollections.observableArrayList();
    private final ObservableList<String> exAndLog = FXCollections.observableArrayList("Exception Stacktrace", "Logs");
    private final ObservableList<String> exStacktrace = FXCollections.observableArrayList("Exception Stacktrace");
    @FXML
    private TableColumn<RuleResult, String> exceptionCol;
    private ObservableList<RuleResult> filteredResults = FXCollections.observableArrayList();
    private Inspector inspector;
    private final ObservableList<String> logs = FXCollections.observableArrayList("Logs");
    @FXML
    private TableColumn<RuleResult, Integer> logsCol;
    @FXML
    private Label nameLabel;
    @FXML
    private Label notRanLabel;
    @FXML
    private Label numberOfExceptionsLabel;
    @FXML
    private Label numberOfLogsLabel;
    @FXML
    private TableColumn<RuleResult, Boolean> ranCol;
    @FXML
    private Label ranLabel;
    @FXML
    private ChoiceBox<String> ruleChoiceBox;
    private ObservableList<String> ruleChoices = FXCollections.observableArrayList();
    @FXML
    private TableView<RuleResult> ruleExecutionTable;
    private RuleResult selectedResult;
    @FXML
    private TableColumn<RuleResult, String> startTimeCol;
    @FXML
    private Label totalExecutionsLabel;
    @FXML
    private Label totalRunTimeLabel;
    @FXML
    private TextArea viewArea;
    @FXML
    private ChoiceBox<String> viewChoiceBox;

    /**
     * Filters the table to only show rules with exceptions.
     */
    public void filterExceptions() {
        setupColumns();
        ruleExecutionTable.setItems(filteredResults.filtered(r -> exceptionResult(r.getEx()).equalsIgnoreCase("yes")));
    }

    /**
     * Filters the table to only show rules that did not run.
     */
    public void filterNotRan() {
        setupColumns();
        ruleExecutionTable.setItems(filteredResults.filtered(r -> String.valueOf(r.getRan()).equalsIgnoreCase("false")));
    }

    /**
     * Filters the table to only show rules that ran.
     */
    public void filterRan() {
        setupColumns();
        ruleExecutionTable.setItems(filteredResults.filtered(r -> String.valueOf(r.getRan()).equalsIgnoreCase("true")));
    }

    /**
     * Links the controller to the main application.
     *
     * @param inspector
     */
    public void setInspector(Inspector inspector) {
        this.inspector = inspector;

        // -- Setup rule choice box -- //
        ruleChoiceBox.setItems(inspector.getStatistics().getRuleChoices());
        ruleChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> setRule(newValue));

    }

    /**
     * Removes filters from table to show all entries.
     */
    public void unfilter() {
        setupColumns();
        ruleExecutionTable.setItems(filteredResults);
    }

    /**
     * Creates a readable string from the list of input data retrieved from the RuleResult.
     *
     * @return Formatted string of input data
     */
    private String createInput() {
        if (selectedResult.getInputData().size() > 0) {
            StringBuilder sb = new StringBuilder();
            int index = 1;
            for (String input : selectedResult.getInputData()) {
                sb.append("[Input ").append(index).append("] ").append(input).append("\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * Creates a readable string from the list of output data retrieved from the RuleResult.
     *
     * @return Formatted string of output data.
     */
    private String createOutput() {
        if (selectedResult.getOutputData().size() > 0) {
            StringBuilder sb = new StringBuilder();
            int index = 1;
            for (String input : selectedResult.getOutputData()) {
                sb.append("[Output ").append(index).append("] ").append(input).append("\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * Formates an exception to show "Yes" or "No" based on if there is an exception or not.
     *
     * @param ex exception to format.
     * @return formatted string.
     */
    private String exceptionResult(Throwable ex) {
        if (ex == null) {
            return "No";
        } else {
            return "Yes";
        }
    }

    /**
     * Formats duration to an easier to read format.
     *
     * @param duration the duration to format.
     * @return a formatted string.
     */
    private String formatDuration(Duration duration) {
        if (duration.toHours() > 1) {
            long s = duration.getSeconds();
            long n = duration.getNano();

            return String.format("%d:%02d:%02d.%03d", s / 3600, (s % 3600) / 60, (s % 60), TimeUnit.MILLISECONDS.convert(n, TimeUnit.NANOSECONDS));
        } else {
            long s = duration.getSeconds();
            long n = duration.getNano();

            return String.format("%02d:%02d.%03d", s / 60, s % 60, TimeUnit.MILLISECONDS.convert(n, TimeUnit.NANOSECONDS));
        }

    }

    /**
     * Creates a string from an exception's stackTrace
     *
     * @param ex exception to create string from
     * @return formatted string of exception's stackTrace
     */
    private String formatException(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    /**
     * Formats logs into a presentable string.
     *
     * @param logs the logs to format
     * @return a formatted string of logs.
     */
    private String formatLogs(List<String> logs) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        for (String log : logs) {
            sb.append("[Log ").append(index++).append("] ").append(log).append("\n");
        }

        return sb.toString();
    }

    /**
     * Sets various visible elements to correspond with the selected rule.
     *
     * @param ruleName the selected rule name.
     */
    private void setRule(String ruleName) {

        // -- Set Table Data -- //
        setupColumns();

        ruleExecutionTable.setItems(inspector.getRuleResults().filtered(r -> r.getRuleName().equalsIgnoreCase(ruleName)));
        filteredResults = inspector.getRuleResults().filtered(r -> r.getRuleName().equalsIgnoreCase(ruleName));

        // -- Set Values of Summary Labels -- //
        nameLabel.textProperty().bind(new SimpleStringProperty(inspector.getStatistics().getRuleStatisticByName(ruleName).getRuleName()));
        ranLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getTimesRan().asString());
        notRanLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getTimesNotRan().asString());
        totalExecutionsLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getTotalExecutions().asString());
        averageRunTimeLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getAverageDuration());
        totalRunTimeLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getTotalDurationProperty());
        numberOfExceptionsLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getNumberOfExceptions().asString());
        numberOfLogsLabel.textProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getNumberOfLogs().asString());

        // -- Set Chart Data -- //
        durationChart.getData().clear();
        durationChartYAxis.labelProperty().bind(inspector.getStatistics().getRuleStatisticByName(ruleName).getChartYAxisLabel());
        durationChart.getData().add(inspector.getStatistics().getRuleStatisticByName(ruleName).getRuleChartData());

    }

    /**
     * Sets the data in the view area to blank, the exception or logs depending on choice box value.
     *
     * @param view the choice box value.
     */
    private void setView(String view) {
        if (view == null) {
            viewArea.setText("");
        } else if (view.equalsIgnoreCase("Exception Stacktrace")) {
            viewArea.setText(formatException(selectedResult.getEx()));
        } else if (view.equalsIgnoreCase("Logs")) {
            viewArea.setText(formatLogs(selectedResult.getLog()));
        } else if (view.equalsIgnoreCase("Input")) {
            viewArea.setText(createInput());
        } else if (view.equalsIgnoreCase("Output")) {
            viewArea.setText(createOutput());
        }
    }

    /**
     * Loads when the view is initialized. sets labels to blank, assigns data to choice boxes, and assigns data to columns in the table.
     */
    @FXML
    private void initialize() {

        durationChart.setAnimated(false);
        setLabelsBlank();

        ruleExecutionTable.setPlaceholder(new Text("No Executions."));

        // -- Formatter for time -- //
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.US)
                .withZone(ZoneId.systemDefault());

        // -- Assign columns data -- //
        ranCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().getRan()));
        startTimeCol.setCellValueFactory(cellData -> new SimpleStringProperty(formatter.format(cellData.getValue().getTimeRan())));
        durationCol.setCellValueFactory(cellData -> new SimpleStringProperty(formatDuration(cellData.getValue().getDuration())));
        exceptionCol.setCellValueFactory(cellData -> new SimpleStringProperty(exceptionResult(cellData.getValue().getEx())));
        logsCol.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getLog().size()).asObject());

        // -- Chart -- //
        durationChartXAxis.setLabel("Execution");
        durationChartYAxis.setLabel("Run Time");
        durationChart.setTitle("Execution Run Times");
        durationChart.setLegendVisible(false);

        // -- Choice Box -- //
        viewChoiceBox.setDisable(true);
        viewChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldvalue, newValue) -> setView(newValue));

        // -- Add Selection Listener to Table -- //
        ruleExecutionTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showLogsAndExceptions(newValue));

    }

    /**
     * Sets all labels to blank.
     */
    private void setLabelsBlank() {
        nameLabel.setText("");
        ranLabel.setText("");
        notRanLabel.setText("");
        totalExecutionsLabel.setText("");
        averageRunTimeLabel.setText("");
        totalRunTimeLabel.setText("");
        numberOfExceptionsLabel.setText("");
        numberOfLogsLabel.setText("");
    }

    /**
     * Adds the filtered data to the table.
     */
    private void setupColumns() {
        // Add sorted (and filtered) data to the table.
        ruleExecutionTable.setItems(inspector.getMonitorController().getResultsTable().getItems());
    }

    /**
     * sets the values of the view choice box based on the selected RuleResult.
     *
     * @param ruleResult
     */
    private void showLogsAndExceptions(RuleResult ruleResult) {
        if (ruleResult != null) {
            selectedResult = ruleResult;
            // setup choicebox
            if (ruleResult.getEx() != null & ruleResult.getLog().size() > 0) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(exAndLog);
                viewChoiceBox.setValue("Exception Stacktrace");
            } else if (ruleResult.getEx() != null & ruleResult.getLog().size() < 1) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(exStacktrace);
                viewChoiceBox.setValue("Exception Stacktrace");
            } else if (ruleResult.getEx() == null & ruleResult.getLog().size() > 0) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(logs);
                viewChoiceBox.setValue("Logs");
            } else if (ruleResult.getEx() == null & ruleResult.getLog().size() < 1) {
                viewChoiceBox.setDisable(false);
                viewChoiceBox.setItems(empty);
//                viewChoiceBox.setValue("Input");

            } else {
                viewChoiceBox.setDisable(true);
            }
        }
    }

}
