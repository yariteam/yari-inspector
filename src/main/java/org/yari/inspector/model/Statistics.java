/* 
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.model;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import org.yari.core.ExecutionResult;
import org.yari.core.RuleResult;

public class Statistics {

    private ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
    private ObservableList<String> ruleChoices = FXCollections.observableArrayList();
    private ObservableList<RuleStatistic> ruleStatistics = FXCollections.observableArrayList();
    private Map<String, DoubleProperty> runTimePercents = new HashMap<>();
    private IntegerProperty totalExecutions = new SimpleIntegerProperty();
    private Duration totalRunTime;

    public Statistics() {
        this.totalRunTime = Duration.ZERO;
    }

    /**
     * Adds an entire ExectionResult to the statistics.
     *
     * @param executionResult
     */
    public void addExecutionResult(ExecutionResult executionResult) {
        for (RuleResult ruleResult : executionResult.getRuleResults()) {
            addRuleResult(ruleResult);
        }
    }

    /**
     * Searches the RuleStatistics for a matching statistic, if not found, creates a new statistic and adds the rule, if found, adds the rule to the proper
     * statistic.
     *
     * @param ruleResult the RuleResult to add to statistics.
     */
    public void addRuleResult(RuleResult ruleResult) {
        boolean found = false;

        if (ruleStatistics.size() < 1) {
            RuleStatistic newStat = new RuleStatistic();
            newStat.addResult(ruleResult);
            newStat.setRuleName(ruleResult.getRuleName());
            ruleStatistics.add(newStat);
            ruleChoices.add(newStat.getRuleName());
            SimpleDoubleProperty percent = new SimpleDoubleProperty();
            percent.setValue(calculatePercentRunTime(newStat));
            runTimePercents.put(ruleResult.getRuleName(), percent);
        } else {
            for (RuleStatistic stats : ruleStatistics) {
                if (stats.getRuleName().equals(ruleResult.getRuleName())) {
                    stats.addResult(ruleResult);
                    SimpleDoubleProperty percent = new SimpleDoubleProperty();
                    percent.setValue(calculatePercentRunTime(stats));
                    runTimePercents.put(ruleResult.getRuleName(), percent);
                    found = true;
                    break;
                }
            }
            if (!found) {
                RuleStatistic newStat = new RuleStatistic();
                newStat.addResult(ruleResult);
                newStat.setRuleName(ruleResult.getRuleName());
                ruleStatistics.add(newStat);
                ruleChoices.add(newStat.getRuleName());
                SimpleDoubleProperty percent = new SimpleDoubleProperty();
                percent.setValue(calculatePercentRunTime(newStat));
                runTimePercents.put(ruleResult.getRuleName(), percent);
            }
        }
        totalRunTime = totalRunTime.plus(ruleResult.getDuration());
        int temp = totalExecutions.getValue();
        temp++;
        totalExecutions.setValue(temp);
        updatePieChartData();
    }

    // -- Getters and Setters -- //
    public void addRuleStatistic(RuleStatistic ruleStatistic) {
        this.ruleStatistics.add(ruleStatistic);
    }

    public ObservableList<PieChart.Data> getPieChartData() {
        return pieChartData;
    }

    public ObservableList<String> getRuleChoices() {
        return ruleChoices;
    }

    /**
     * Returns a RuleStatistic that matches a rule name.
     *
     * @param ruleName Rule name to search for.
     * @return
     */
    public RuleStatistic getRuleStatisticByName(String ruleName) {
        for (RuleStatistic stat : ruleStatistics) {
            if (stat.getRuleName().equals(ruleName)) {
                return stat;
            }
        }
        return null;
    }

    public ObservableList<RuleStatistic> getRuleStatistics() {
        return this.ruleStatistics;
    }

    public Map<String, DoubleProperty> getRunTimePercents() {
        return runTimePercents;
    }

    public IntegerProperty getTotalExecutions() {
        return totalExecutions;
    }

    /**
     * Updates the pie chart data to reflect any new statistics.
     */
    public void updatePieChartData() {
        ObservableList<PieChart.Data> chartData = FXCollections.observableArrayList();
        for (RuleStatistic stats : ruleStatistics) {
            double percent = ((double) stats.getTimesNotRan().getValue() + (double) stats.getTimesRan().getValue()) / (double) totalExecutions.get();
            chartData.add(new PieChart.Data(stats.getRuleName(), percent));
        }
        this.pieChartData = chartData;
    }

    /**
     * Calculates the percent run time for a RuleStatistic.
     *
     * @param ruleStatistic
     * @return
     */
    private double calculatePercentRunTime(RuleStatistic ruleStatistic) {
        double runTime = 0;
        for (RuleStatistic stats : ruleStatistics) {
            runTime += stats.getTotalDuration().getNano();
        }
        double y = ruleStatistic.getTotalDuration().getNano();
        double result = y / runTime;
        return (Math.round(result * 100.0) / 100.0) * 100;
    }

}
