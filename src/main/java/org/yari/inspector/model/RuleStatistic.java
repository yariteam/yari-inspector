/* 
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.model;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.chart.XYChart;
import org.yari.core.RuleResult;

public class RuleStatistic {

    private String ruleName;
    private IntegerProperty timesNotRan = new SimpleIntegerProperty();
    private IntegerProperty timesRan = new SimpleIntegerProperty();
    private IntegerProperty totalExecutions = new SimpleIntegerProperty();
    private StringProperty averageDuration = new SimpleStringProperty();
    private StringProperty totalDurationProperty = new SimpleStringProperty();
    private XYChart.Series ruleChartData = new XYChart.Series();
    private Duration avgDuration;
    private ChartTimes chartTimeFormat;
    private int chartIndex = 1;
    private StringProperty chartYAxisLabel = new SimpleStringProperty();
    private ChartTimes checkChanges = ChartTimes.NANOS;

    private Duration totalDuration;
    private IntegerProperty numberOfExceptions = new SimpleIntegerProperty();
    private IntegerProperty numberOfLogs = new SimpleIntegerProperty();
    private List<RuleResult> ruleResults = new ArrayList<>();

    public RuleStatistic() {
        totalDuration = Duration.ZERO;
    }

    /**
     * Adds a RuleResult to the statistics.
     *
     * @param ruleResult
     */
    public void addResult(RuleResult ruleResult) {
        // -- Times Ran/Not Ran -- //
        if (ruleResult.getRan()) {
            int temp = timesRan.getValue();
            temp++;
            timesRan.setValue(temp);
        } else {
            int temp = timesNotRan.getValue();
            temp++;
            timesNotRan.setValue(temp);
        }
        totalExecutions.setValue(timesRan.getValue() + timesNotRan.getValue());

        // -- Exceptions -- //
        if (ruleResult.getEx() != null) {
            int temp = numberOfExceptions.getValue();
            temp++;
            numberOfExceptions.setValue(temp);
        }

        // -- Logs -- //
        if (ruleResult.getLog() != null) {
            numberOfLogs.setValue(numberOfLogs.getValue() + ruleResult.getLog().size());
        }
        // -- Add Result to List -- //
        ruleResults.add(ruleResult);

        // -- Add to totalDuration -- //
        totalDuration = totalDuration.plus(ruleResult.getDuration());

        updateAverageDuration();
        updateTotalDuration();

        updateChartData(ruleResult);
    }

    // -- Getters and Setters -- //
    public XYChart.Series getRuleChartData() {
        return ruleChartData;
    }

    public Duration getTotalDuration() {
        return totalDuration;
    }

    public StringProperty getTotalDurationProperty() {
        return totalDurationProperty;
    }

    private void updateTotalDuration() {
        this.totalDurationProperty.setValue(formatDuration(totalDuration));
    }

    public IntegerProperty getTotalExecutions() {
        return totalExecutions;
    }

    public StringProperty getAverageDuration() {
        return averageDuration;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public IntegerProperty getNumberOfLogs() {
        return numberOfLogs;
    }

    public IntegerProperty getTimesRan() {
        return timesRan;
    }

    public IntegerProperty getTimesNotRan() {
        return timesNotRan;
    }

    /**
     * Updates the average duration with current data.
     */
    private void updateAverageDuration() {
        if (!((timesRan.getValue() + timesNotRan.getValue()) == 0)) {

            this.averageDuration.setValue(formatDuration(totalDuration.dividedBy((timesRan.getValue() + timesNotRan.getValue()))));
            this.avgDuration = totalDuration.dividedBy((timesRan.getValue() + timesNotRan.getValue()));
        }
    }

    /**
     * Formats duration to an easier to read format.
     *
     * @param duration the duration to format.
     * @return a formatted string.
     */
    private String formatDuration(Duration duration) {
        if (duration.toHours() > 1) {
            long s = duration.getSeconds();
            long n = duration.getNano();

            return String.format("%d:%02d:%02d.%03d", s / 3600, (s % 3600) / 60, (s % 60), TimeUnit.MILLISECONDS.convert(n, TimeUnit.NANOSECONDS));
        } else {
            long s = duration.getSeconds();
            long n = duration.getNano();

            return String.format("%02d:%02d.%03d", s / 60, s % 60, TimeUnit.MILLISECONDS.convert(n, TimeUnit.NANOSECONDS));
        }

    }

    /**
     * Updates chart data with a new RuleResult.
     *
     * @param rr
     */
    public void updateChartData(RuleResult rr) {
        // Check if the time format needs to change..
        determineChartFormat();
        // set the chart data name..
        ruleChartData.setName(ruleName);

        // check if whole chart needs to be re-drawn:
        if (chartTimeFormat != checkChanges) {
            checkChanges = chartTimeFormat;
            ruleChartData.getData().clear();
            // Check if the size is greater than 25 and redraw last 25.
            if (ruleResults.size() > 25) {
                for (int i = ruleResults.size() - 25; i < ruleResults.size(); i++) {
                    long time = convertTimeForChart(ruleResults.get(i).getDuration().toNanos());
                    ruleChartData.getData().add(new XYChart.Data(chartIndex++, time));
                }
                // Redraw whats available.
            } else {
                chartIndex = 0;
                for (RuleResult ruleResult : ruleResults) {
                    long time = convertTimeForChart(ruleResult.getDuration().toNanos());
                    ruleChartData.getData().add(new XYChart.Data(chartIndex++, time));
                }

            }
            // if it doesn't need to be re-drawn, simply add the new result:
        } else if (ruleResults.size() > 25) {

            ruleChartData.getData().clear();
            chartIndex = 0;
            for (int i = ruleResults.size() - 25; i < ruleResults.size(); i++) {
                long time = convertTimeForChart(ruleResults.get(i).getDuration().toNanos());
                ruleChartData.getData().add(new XYChart.Data(chartIndex++, time));
            }

        } else {
            long time = convertTimeForChart(rr.getDuration().toNanos());
            ruleChartData.getData().add(new XYChart.Data(chartIndex++, time));
        }

    }

    public StringProperty getChartYAxisLabel() {
        return chartYAxisLabel;
    }

    /**
     * Converts nanoseconds to proper time format for the chart.
     *
     * @param nano
     * @return
     */
    private long convertTimeForChart(long nano) {
        switch (chartTimeFormat) {
            case NANOS:
                this.chartYAxisLabel.setValue("Run Time (ns)");
                return nano;
            case MILLISECONDS:
                this.chartYAxisLabel.setValue("Run Time (ms)");
                return TimeUnit.MILLISECONDS.convert(nano, TimeUnit.NANOSECONDS);
            case SECONDS:
                this.chartYAxisLabel.setValue("Run Time (s)");
                return TimeUnit.SECONDS.convert(nano, TimeUnit.NANOSECONDS);
            case MINUTES:
                this.chartYAxisLabel.setValue("Run Time (m)");
                return TimeUnit.MINUTES.convert(nano, TimeUnit.NANOSECONDS);
            case HOURS:
                this.chartYAxisLabel.setValue("Run Time (h)");
                return TimeUnit.SECONDS.convert(nano, TimeUnit.NANOSECONDS);
        }
        return 0;
    }

    /**
     * Available time units for the chart.
     */
    private enum ChartTimes {
        NANOS, MILLISECONDS, SECONDS, MINUTES, HOURS
    }

    /**
     * Determines which time unit to convert data to for line chart based on the average duration of the rule run time.
     */
    private void determineChartFormat() {
        if (avgDuration.toHours() > 1) {
            chartTimeFormat = ChartTimes.HOURS;
        } else if (avgDuration.toMinutes() > 1) {
            chartTimeFormat = ChartTimes.MINUTES;
        } else if ((avgDuration.getSeconds() > 1)) {
            chartTimeFormat = ChartTimes.SECONDS;
        } else {
            chartTimeFormat = ChartTimes.MILLISECONDS;
        }
    }

    public IntegerProperty getNumberOfExceptions() {
        return numberOfExceptions;
    }

    public List<RuleResult> getRuleResults() {
        return ruleResults;
    }

    public void setRuleResults(List<RuleResult> ruleResults) {
        this.ruleResults = ruleResults;
    }

}
