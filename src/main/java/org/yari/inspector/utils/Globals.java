/* 
 * This file is part of Yari.
 *
 * Yari is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yari is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yari.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.yari.inspector.utils;

import javafx.scene.image.Image;

/**
 *
 * @author 600848
 */
public class Globals {

    public static final Image YARI_LOGO = new Image("/images/YariLogo.png");
}
