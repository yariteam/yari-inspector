# Yari Inspector #

Yari Inspector provides a graphical interface for users of the Yari rules engine to visualize various information such as: what rules are being fired and when, log messages, exceptions, run times and more.

### Build Yari Inspector ###
To build Yari Inspector, navigate to it's directory and use the command:
```
mvn package
```

### Run Yari Inspector ###
Go to the [downloads page](https://bitbucket.org/yariteam/yari-inspector/wiki/Download) to get the latest release of Yari Inspector. Alternatively, you can launch Yari Inspector from the executable Jar file created during the build.

### Documentation ###
For documentation on how to use Yari Inspector, [see our wiki on bitbucket](https://bitbucket.org/yariteam/yari-inspector/wiki/Home).

### License ###
Yari Inspector is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Yari Inspector is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Yari Inspector. If not, see http://www.gnu.org/licenses.
